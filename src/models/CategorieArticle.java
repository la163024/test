package models;

import java.util.ArrayList;

public class CategorieArticle {
    private String name;
    @Override
    public String toString() {
        return "CategorieArticle{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public CategorieArticle(String name)
    {

        this.name=name;
    }
}
