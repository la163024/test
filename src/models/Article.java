package models;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Article {

    private String nomArticle;
    private double prixArticle;
    private boolean isAuKilo;
    ArrayList<Article> listArticles;
    ArrayList<CategorieArticle>listCategorie;
    ArrayList<String>listNomCategorie=new ArrayList<>();

    public ArrayList<CategorieArticle> getListCategorie() {
        return listCategorie;
    }


    File file=new File("D:/Bureau/Java/projetq2test/src/controllers/test.txt");

    public File getFile() {
        return file;
    }

    public ArrayList<Article> getListArticles() {
        return listArticles;
    }

    public void lireFichier() throws FileNotFoundException {
        listArticles=new ArrayList<>();
        listCategorie=new ArrayList<>();
        Scanner sc=new Scanner(file);
        boolean isAuKilo;
        while(sc.hasNextLine())
        {
            String line=sc.nextLine();
            String[] details=line.split(":");
            String nomCategorie=details[0];
            String nomArticle=details[1];
            if(!listNomCategorie.contains(nomCategorie))
            {
                listNomCategorie.add(nomCategorie);
            }
            double prixArticle=Double.parseDouble(details[2]);
            String auKilo=details[3];
            if(details[2].equals("kilo"))
            {
                isAuKilo=true;
            }else isAuKilo=false;
            Article article=new Article(nomArticle,prixArticle,isAuKilo);
            listArticles.add(article);
        }
        for(String name:listNomCategorie)
        {
            CategorieArticle categorieArticle=new CategorieArticle(name);
            listCategorie.add(categorieArticle);
        }
        System.out.println(listCategorie);
    }

    public void setAuKilo(boolean auKilo) {
        isAuKilo = auKilo;
    }

    @Override
    public String toString() {
        return "nom: "+nomArticle +" prix: "+prixArticle+" au Kilo? "+isAuKilo+"\n";
    }
public Article(){}

    public Article(String nomArticle, double prixArticle, boolean isAuKilo)  {
        this.nomArticle=nomArticle;
        this.prixArticle=prixArticle;
        this.isAuKilo=isAuKilo;
    }

    public String getNomArticle() {
        return nomArticle;
    }

    public double getPrixArticle() {
        return prixArticle;
    }

    public boolean isAuKilo() {
        return isAuKilo;
    }
}
