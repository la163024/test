package controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import views.ViewController;


public class MainController extends Application{
    private ViewController viewController;
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/interfaceClient.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        viewController=loader.getController();
        viewController.setMainController(this);
        primaryStage.setTitle("Vue traiteur");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
