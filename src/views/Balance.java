package views;

import controllers.MainController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;

public class Balance extends Observable implements Initializable{

    @FXML private TextField poidsBalance;
    public TextField getPoidsBalance() {
        return poidsBalance;
    }

    @FXML void onKeyReleased(KeyEvent event)
    {
        setChanged();
        notifyObservers();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
