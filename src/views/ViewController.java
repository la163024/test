package views;

import controllers.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import models.Article;
import models.CategorieArticle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

public class ViewController implements Initializable,Observer{
    private Balance balance;
    private Article article=new Article();
    private MainController mainController;

    @FXML private VBox VBox;
    @FXML private Label nomArticleGauche;
    @FXML private Label prixArticleGauche;
    @FXML private Label poidsArticleGauche;
    @FXML private VBox VBoxCommande;
    @FXML private Label prixCommande;
    @FXML private ComboBox<Integer> comboBoxPiece;

    private boolean isAuKiloController=false;
    private double total=0;

    public void setMainController(MainController mainController)
    {
        this.mainController=mainController;
    }

    public static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }
    @FXML
    void ajouterALaCommande(ActionEvent event) {

        Label nomArticle=new Label(nomArticleGauche.getText());
        Label prixArticle=new Label("");
        Label poidsArticle=new Label("");
        double poidsArticle2=Double.parseDouble(removeLastChar(poidsArticleGauche.getText()));
        double prixArticle2=Double.parseDouble(removeLastChar(prixArticleGauche.getText()));
        if(isAuKiloController)
        {
            double poidsArticleCalcule=poidsArticle2/1000;
            double prixArticleCalcule=prixArticle2/poidsArticleCalcule;
            poidsArticle.setText(poidsArticleCalcule+"kg à "+prixArticleCalcule+"€/kg");
            prixArticle.setText(prixArticle2+"€");
        }
        else
        {
            int nbArticle=comboBoxPiece.getValue();
            double prixArticleCalcule=prixArticle2*nbArticle;
            poidsArticle.setText(nbArticle+"p à "+prixArticle2+"€/p");
            prixArticle.setText(prixArticleCalcule+"€");
        }
        total+=prixArticle2;
        VBoxCommande.getChildren().addAll(nomArticle,poidsArticle,prixArticle);
        prixCommande.setText(total+"€");
        balance.getPoidsBalance().clear();
        poidsArticleGauche.setText("0g");
    }

    class CreerCategorieArticle
    {
        String nomCategorie;
        FlowPane flowPane;

        public FlowPane getFlowPane() {
            return flowPane;
        }

        public CreerCategorieArticle(String nomCategorie)
        {
            this.nomCategorie=nomCategorie;
            TitledPane titledPane=new TitledPane();
            titledPane.setText(nomCategorie);
            flowPane=new FlowPane();
            flowPane.setPadding(new Insets(5,5,5,5));
            VBox.getChildren().addAll(titledPane,flowPane);
            flowPane.getChildren().add(boutonCreerArticle());
        }
    }

    private Button boutonCreerArticle()
    {
        StackPane stackPane=new StackPane();
        Button button=new Button();
        button.setPrefSize(125,125);
        Image image= new Image(getClass().getResourceAsStream("plus.PNG"));
        button.setGraphic(new ImageView(image));
        button.setContentDisplay(ContentDisplay.TOP);
        button.setText("Ajouter un article");
        button.setAlignment(Pos.CENTER);
        stackPane.getChildren().add(button);
        VBox.getChildren().add(button);
        return button;
    }

    private Button creerBoutonArticle(String nomArticle, double prixArticle, boolean isAuKilo)
    {
        Button button=new Button();
        button.setPrefSize(125,125);
        Image image= new Image(getClass().getResourceAsStream("default.png"));
        button.setGraphic(new ImageView(image));
        button.setContentDisplay(ContentDisplay.TOP);
        button.setTextAlignment(TextAlignment.CENTER);
        if(isAuKilo)
        {
            button.setText(nomArticle+"\n"+prixArticle+"€/kg");
        }
        else
        {
            button.setText(nomArticle+"\n"+prixArticle+"€/p");
        }

        button.setOnAction(event ->
                {
                    double poidsArticle=Double.parseDouble(removeLastChar(poidsArticleGauche.getText()));
                    nomArticleGauche.setText(nomArticle);

                    if(isAuKilo)
                    {
                        prixArticleGauche.setText((prixArticle*poidsArticle/1000)+"€");
                        comboBoxPiece.setVisible(false);
                        isAuKiloController=true;
                    }
                    else
                    {
                        prixArticleGauche.setText((prixArticle)+"€");
                        comboBoxPiece.setVisible(true);
                        isAuKiloController=false;
                    }
                }
        );
        return button;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chargerBalance();
        for(int i=0;i<30;i++)
        {
            comboBoxPiece.getItems().addAll(i+1);
        }
        comboBoxPiece.setVisible(false);

        //lire fichier pour récupérer les articles
        getFile();
        CreerCategorieArticle viandes=new CreerCategorieArticle("Viandes");
        for(Article articles:article.getListArticles())
        {
            viandes.getFlowPane().getChildren().add(0, creerBoutonArticle(articles.getNomArticle(),articles.getPrixArticle(),articles.isAuKilo()));
        }


        for(CategorieArticle categorieArticle:article.getListCategorie())
        {
            CreerCategorieArticle creerCategorieArticle=new CreerCategorieArticle(categorieArticle.getName());
        }


    }

    public void getFile()
    {
        article.getFile();
        try {
            article.lireFichier();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void chargerBalance()
    {
        Stage stageBalance=new Stage();
        FXMLLoader loader2 = new FXMLLoader(getClass().getResource("../views/balance.fxml"));
        try {
            loader2.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root2=loader2.getRoot();
        balance=loader2.getController();
        balance.addObserver(this);
        stageBalance.setTitle("Balance");
        stageBalance.setScene(new Scene(root2));
        stageBalance.setX(1100);
        stageBalance.show();
    }

    @Override
    public void update(Observable o, Object arg) {
        poidsArticleGauche.setText(balance.getPoidsBalance().getText()+"g");
    }
}

